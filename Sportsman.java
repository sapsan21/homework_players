public class Sportsman {

    private String name;

    private Integer age;

    private Integer medialCount;

    public Sportsman(String name, Integer age, Integer medialCount) {
        this.name = name;
        this.age = age;
        this.medialCount = medialCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getMedialCount() {
        return medialCount;
    }

    public void setMedialCount(Integer medialCount) {
        this.medialCount = medialCount;
    }
}
